<?php

use App\Contracts\ShopContract;
use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\Auth\AuthAdminController;
use App\Http\Controllers\Api\Auth\AuthCompanyController;
use App\Http\Controllers\Api\Auth\AuthShopController;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\ShopController;
use App\Http\Controllers\PublicController;
use App\Models\Company;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [PublicController::class, 'getCategories'])->name('public.categories');
        Route::get('/{category}/products', [PublicController::class, 'getProductsByCategory'])->name('public.categories.products');
    });

    Route::get('company/{company}/products', [PublicController::class, 'getProductsByCompany'])->name('public.company.products');
    Route::post('shoppingcart', [PublicController::class, 'getShoppingCart'])->name('public.shoppingcart');

    Route::group(['prefix' => 'products'], function () {
        Route::get('/{product}', [PublicController::class, 'getProduct'])->name('public.product');
        Route::post('/search', [PublicController::class, 'searchProduct'])->name('public.search');
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::post('login', [AuthAdminController::class, 'login'])->name('admin.login');

        Route::group(['middleware' => 'admin:admin-api'], function () {
            Route::post('logout', [AuthAdminController::class,'logout'])->name('admin.logout');
            Route::post('refresh', [AuthAdminController::class,'refresh'])->name('admin.refresh');
            Route::post('me', [AuthAdminController::class,'me'])->name('admin.me');
          
            Route::group(['prefix' => 'category'], function () {
                Route::get('/', [AdminController::class, 'getParentCategoriesWithChildWithProductCount'])->name('admin.parent.category.child.product.count');
                Route::post('/', [AdminController::class, 'storeCategory'])->name('admin.store.child.category');
                Route::post('/{category}', [AdminController::class, 'updateCategory'])->name('admin.update.child.category');
                Route::delete('/{category}', [AdminController::class, 'destroyCategory'])->name('admin.destroy.category');
                Route::get('/parent', [AdminController::class, 'getParentCategories'])->name('admin.parent.category');
            });

            Route::group(['prefix' => 'companies'], function () {
                Route::get('/', [AdminController::class, 'getCompaniesWithProductsCountAndOrdersCount'])->name('admin.companies.with.products.orders.count');
                Route::post('/{company}/block', [AdminController::class, 'blockCompany'])->name('admin.company.block');
                Route::get('/{company}/products', [AdminController::class, 'getCompanyProducts'])->name('admin.company.products');
                Route::get('/{company}/orders', [AdminController::class, 'getCompanyOrders'])->name('admin.company.orders');
            
                Route::group(['prefix' => 'products'], function () {
                    Route::post('/{product_company}/approve', [AdminController::class, 'approveCompaniesProducts'])->name('admin.company.product.approve');
                });
            });

            Route::group(['prefix' => 'shops'], function () {
                Route::get('/', [AdminController::class, 'getAllShopsWithOrdersCount'])->name('admin.shops.with.order.count');
                Route::post('/{shop}/block', [AdminController::class, 'blockShop'])->name('admin.shops.block');
                Route::get('/{shop}/orders', [AdminController::class, 'getShopOrders'])->name('admin.shops.orders');
            });

            Route::group(['prefix' => 'products'], function () {
                Route::get('/', [AdminController::class, 'getProductsWithCategoriesWithCompanyProduct'])->name('admin.products');
                Route::post('/{product}/approve', [AdminController::class, 'approveProduct'])->name('admin.products.approve');
            });
        });
    });
 
    Route::group(['prefix' => 'company'], function () {
        Route::post('login', [AuthCompanyController::class, 'login'])->name('company.login');
        Route::post('register', [AuthCompanyController::class, 'register'])->name('company.register');

        Route::group(['middleware' => 'company:company-api'], function () {
            Route::post('logout', [AuthCompanyController::class, 'logout'])->name('company.logout');
            Route::post('refresh', [AuthCompanyController::class, 'refresh'])->name('company.refresh');
            Route::get('me', [AuthCompanyController::class, 'me'])->name('company.me');
            Route::post('update', [AuthCompanyController::class, 'update'])->name('company.update');

            Route::get('categories', [CompanyController::class, 'getParentCategoriesWithChildCategories'])->name('company.get.categories');
            
            Route::group(['prefix' => 'products'], function () {
                Route::post('/create', [CompanyController::class, 'storeProduct'])->name('company.products.store');
                Route::post('/connect', [CompanyController::class, 'connectToProduct'])->name('company.products.connect');
                Route::post('/{product_company}/update', [CompanyController::class, 'updateProductCompany'])
                    ->middleware('can:companyAccess,product_company')
                    ->name('company.products.update');
                Route::get('/all', [CompanyController::class, 'getApprovedProducts'])->name('company.products.all');
                Route::get('/my', [CompanyController::class, 'getCompanyProducts'])->name('company.products.my');

                Route::group(['prefix' => 'search'], function () {
                    Route::post('/my', [CompanyController::class, 'searchProductOfCompany'])->name('company.products.search.my');
                    Route::post('/all', [CompanyController::class, 'searchProductForConnect'])->name('company.products.search.all');
                });
            });

            Route::group(['prefix' => 'orders'], function () {
                Route::post('/{order}/accept', [CompanyController::class, 'orderAccept'])
                    ->middleware('can:companyAccess,order')
                    ->name('company.orders.accept');
                Route::get('/{order}/download', [CompanyController::class, 'downloadCompanyOrder'])
                    ->middleware('can:companyAccess,order')
                    ->name('company.orders.download');
                Route::get('/', [CompanyController::class, 'getCompanyOrders'])->name('company.orders');
            });

            Route::group(['prefix' => 'price-list'], function() {
                Route::get('/export', [CompanyController::class, 'exportPriceList']);
                Route::post('/import', [CompanyController::class, 'importPriceList']);
            });
        });
    });

    Route::group(['prefix' => 'shop'], function () {
        Route::post('login', [AuthShopController::class, 'login'])->name('shop.login');
        Route::post('register', [AuthShopController::class, 'register'])->name('shop.register');

        Route::group(['middleware' => 'shop:shop-api'], function () {
            Route::post('logout', [AuthShopController::class, 'logout'])->name('shop.logout');
            Route::post('refresh', [AuthShopController::class, 'refresh'])->name('shop.refresh');
            Route::post('me', [AuthShopController::class, 'me'])->name('shop.me');
            Route::post('destroy', [AuthShopController::class, 'destroy'])->name('shop.destroy');

            Route::group(['prefix' => 'orders'], function () {
                Route::get('/', [ShopController::class, 'getShopOrders'])->name('shop.orders.get');
                Route::post('/', [ShopController::class, 'storeShopOrder'])->name('shop.orders.store');
                Route::post('/{order}/cancel', [ShopController::class, 'cancelShopOrder'])
                    ->middleware('can:shopAccess,order')
                    ->name('shop.orders.cancel');
                Route::post('/{order}/confirm', [ShopController::class, 'confirmShopOrder'])
                    ->middleware('can:shopAccess,order')
                    ->name('shop.orders.confirm');
            });
            
        });
    });
});