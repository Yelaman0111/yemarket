<?php

namespace Database\Factories;

use App\Contracts\AdminContract;
use App\Contracts\ShopContract;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Shop>
 */
class ShopFactory extends Factory
{
    protected $model = Shop::class;
    
    public function definition(): array
    {
        return [
            ShopContract::FIELD_NAME        => $this->faker->name,
            ShopContract::FIELD_PASSWORD    => Hash::make('0987346519'),
            ShopContract::FIELD_PHONE       => $this->faker->phoneNumber(),
            ShopContract::FIELD_ADDRESS     => $this->faker->address(),
            ShopContract::FIELD_BLOCKED     => ShopContract::BLOCKED_FALSE,
        ];
    }
}
