<?php

namespace Database\Factories;

use App\Contracts\ProductCompanyContract;
use App\Models\Company;
use App\Models\Product;
use App\Models\ProductCompany;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductCompanyFactory extends Factory
{
    protected $model = ProductCompany::class;

    public function definition(): array
    {
        return [
            ProductCompanyContract::FIELD_COMPANY_ID => $this->faker->numberBetween(1, 10),
            ProductCompanyContract::FIELD_PRODUCT_ID => $this->faker->numberBetween(1, 50),
            ProductCompanyContract::FIELD_PRICE => $this->faker->numberBetween(1000, 5000),
            ProductCompanyContract::FIELD_SKU  => $this->faker->unique()->name,
            ProductCompanyContract::FIELD_APPROVED => ProductCompanyContract::APPROVED_TRUE,
        ];
    }
}
