<?php

namespace Database\Factories;

use App\Contracts\CompanyContract;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    protected $model = Company::class;

    public function definition()
    {
        return [
            CompanyContract::FIELD_NAME             => $this->faker->name,
            CompanyContract::FIELD_EMAIL            => $this->faker->unique()->email,
            CompanyContract::FIELD_PASSWORD         => Hash::make('123456'),
            CompanyContract::FIELD_PHONE            => $this->faker->phoneNumber(),
            CompanyContract::FIELD_BLOCKED          => CompanyContract::BLOCKED_FALSE,
            CompanyContract::FIELD_MIN_ORDER_SUM    => 10000,
        ];
    }
}
