<?php

namespace Database\Factories;

use App\Contracts\OrderContract;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            OrderContract::FIELD_COMPANY_ID   => $this->faker->numberBetween(1, 10),
            OrderContract::FIELD_SHOP_ID     => $this->faker->numberBetween(1, 10),
            OrderContract::FIELD_STATUS      => OrderContract::STATUS_CREATED,
        ];
    }

    public function canceled()
    {
        return $this->state(function (array $attributes) {
            return [
                OrderContract::FIELD_COMPANY_ID   => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_SHOP_ID     => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_STATUS      => OrderContract::STATUS_CANCELED,
            ];
        });
    }

    public function accepted()
    {
        return $this->state(function (array $attributes) {
            return [
                OrderContract::FIELD_COMPANY_ID   => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_SHOP_ID     => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_STATUS      => OrderContract::STATUS_ACCEPTED,
            ];
        });
    }

    public function finished()
    {
        return $this->state(function (array $attributes) {
            return [
                OrderContract::FIELD_COMPANY_ID   => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_SHOP_ID     => $this->faker->numberBetween(1, 10),
                OrderContract::FIELD_STATUS      => OrderContract::STATUS_FINISHED,
            ];
        });
    }
}
