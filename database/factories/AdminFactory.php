<?php

namespace Database\Factories;

use App\Contracts\AdminContract;
use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class AdminFactory extends Factory
{
    protected $model = Admin::class;
    
    public function definition(): array
    {
        return [
            AdminContract::FIELD_NAME => $this->faker->name,
            AdminContract::FIELD_EMAIL => 'ebuckridge@yahoo.com',
            AdminContract::FIELD_PASSWORD => Hash::make('0987346519'),
        ];
    }
}
