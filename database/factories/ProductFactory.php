<?php

namespace Database\Factories;

use App\Contracts\ProductContract;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        return [
                ProductContract::FIELD_CATEGORY_ID =>$this->faker->numberBetween(10, 50),
                ProductContract::FIELD_NAME => $this->faker->name,
                ProductContract::FIELD_DESCRIPTIONS => $this->faker->name,
                ProductContract::FIELD_IMAGE => 'product.jpg',
                ProductContract::FIELD_APPROVED => ProductContract::APPROVED_TRUE,
        ];
    }
}
