<?php

namespace Database\Factories;

use App\Contracts\OrderProductContract;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderProduct>
 */
class OrderProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            OrderProductContract::FIELD_ORDER_ID     => $this->faker->numberBetween(1, 10),
            OrderProductContract::FIELD_PRODUCT_ID   => $this->faker->numberBetween(1, 50),
            OrderProductContract::FIELD_COUNT        => $this->faker->numberBetween(1, 10),
            OrderProductContract::FIELD_PRICE        => $this->faker->numberBetween(1000, 5000),
        ];
    }
}
