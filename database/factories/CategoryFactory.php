<?php

namespace Database\Factories;

use App\Contracts\CategoryContract;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    protected $model = Category::class;


    public function definition(): array
    {
        return [
            CategoryContract::FIELD_TITLE => $this->faker->name,
            CategoryContract::FIELD_PARENT_ID => $this->faker->numberBetween(1, 15),
            CategoryContract::FIELD_IMAGE => 'category.jpg',
        ];
    }

    public function parent()
    {
        return $this->state(function (array $attributes) {
            return [
                CategoryContract::FIELD_TITLE => $this->faker->name,
                CategoryContract::FIELD_PARENT_ID => 0,
                CategoryContract::FIELD_IMAGE => 'category.jpg',
            ];
        });
    }
}
