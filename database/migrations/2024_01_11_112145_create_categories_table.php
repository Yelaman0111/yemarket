<?php

use App\Contracts\CategoryContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id(CategoryContract::FIELD_ID);
            $table->unsignedBigInteger(CategoryContract::FIELD_PARENT_ID);
            // $table->foreign(CategoryContract::FIELD_PARENT_ID)
            //     ->references('id')
            //     ->on('categories')
            //     ->onDelete('cascade')
            //     ->default(0);
            $table->string(CategoryContract::FIELD_TITLE);
            $table->string(CategoryContract::FIELD_IMAGE)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
