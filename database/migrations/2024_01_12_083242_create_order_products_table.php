<?php

use App\Contracts\OrderProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(OrderProductContract::FIELD_ORDER_ID);
            $table->foreign(OrderProductContract::FIELD_ORDER_ID)
                ->references('id')
                ->on('orders')
                ->onDelete('cascade')
                ->default(0);
            $table->unsignedBigInteger(OrderProductContract::FIELD_PRODUCT_ID);
            $table->foreign(OrderProductContract::FIELD_PRODUCT_ID)
                ->references('id')
                ->on('products')
                ->onDelete('cascade')
                ->default(0);
            $table->integer(OrderProductContract::FIELD_COUNT);
            $table->integer(OrderProductContract::FIELD_PRICE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_products');
    }
};
