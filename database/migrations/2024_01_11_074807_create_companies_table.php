<?php

use App\Contracts\CompanyContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string(CompanyContract::FIELD_NAME);
            $table->string(CompanyContract::FIELD_EMAIL)->unique();
            $table->string(CompanyContract::FIELD_PASSWORD);
            $table->string(CompanyContract::FIELD_PHONE)->nullable();
            $table->boolean(CompanyContract::FIELD_BLOCKED)->default(CompanyContract::BLOCKED_TRUE);
            $table->integer(CompanyContract::FIELD_MIN_ORDER_SUM);
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
