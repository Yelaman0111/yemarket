<?php

use App\Contracts\ProductCompanyContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_companies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(ProductCompanyContract::FIELD_COMPANY_ID);
            $table->foreign(ProductCompanyContract::FIELD_COMPANY_ID)
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->default(0);
            $table->unsignedBigInteger(ProductCompanyContract::FIELD_PRODUCT_ID);
            $table->foreign(ProductCompanyContract::FIELD_PRODUCT_ID)
                ->references('id')
                ->on('products')
                ->onDelete('cascade')
                ->default(0);
            $table->integer(ProductCompanyContract::FIELD_PRICE);
            $table->string(ProductCompanyContract::FIELD_SKU)->unique();
            $table->boolean(ProductCompanyContract::FIELD_APPROVED)->default(ProductCompanyContract::APPROVED_FALSE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_companies');
    }
};
