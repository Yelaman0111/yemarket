<?php

use App\Contracts\OrderContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(OrderContract::FIELD_COMPANY_ID);
            $table->foreign(OrderContract::FIELD_COMPANY_ID)
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->default(0);
            $table->unsignedBigInteger(OrderContract::FIELD_SHOP_ID);
            $table->foreign(OrderContract::FIELD_SHOP_ID)
                ->references('id')
                ->on('shops')
                ->onDelete('cascade')
                ->default(0);
            $table->enum(OrderContract::FIELD_STATUS, [
                OrderContract::STATUS_CREATED,
                OrderContract::STATUS_ACCEPTED,
                OrderContract::STATUS_FINISHED,
                OrderContract::STATUS_CANCELED,
            ])->default(OrderContract::STATUS_CREATED);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
