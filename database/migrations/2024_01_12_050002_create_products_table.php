<?php

use App\Contracts\ProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(ProductContract::FIELD_CATEGORY_ID);
            $table->foreign(ProductContract::FIELD_CATEGORY_ID)
                ->references('id')
                ->on('categories')
                ->onDelete('cascade')
                ->default(0);
            $table->string(ProductContract::FIELD_NAME);
            $table->text(ProductContract::FIELD_DESCRIPTIONS);
            $table->string(ProductContract::FIELD_IMAGE);
            $table->boolean(ProductContract::FIELD_APPROVED)->default(ProductContract::APPROVED_FALSE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
