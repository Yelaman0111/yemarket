<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\ShopChangeBlockStatusRequest;
use App\Models\Shop;

interface ShopRepositoryInterface
{
    public function changeBlockStatus(ShopChangeBlockStatusRequest $request, Shop $shop);

    public function getAllShopsWithOrdersCount();
}