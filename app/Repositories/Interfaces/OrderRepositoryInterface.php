<?php

namespace App\Repositories\Interfaces;

use App\Models\Company;
use App\Models\Order;
use App\Models\Shop;

interface OrderRepositoryInterface
{
    public function getOrdersByCompany(Company $company);

    public function getOrdersByShop(Shop $shop);
    
    public function accept(Order $order);

    public function checkOrderOwner($company_id, $order_id);

    // public function download(Order $order);

    public function storeOrder($company_id, $user_id);

    public function storeOrderProduct($product_id, $product_count, $price, $order_id);

    public function updateStatus(Order $order, $status);

    public function getOrderByIdAndCompanyId($company_id, $order_id);

}