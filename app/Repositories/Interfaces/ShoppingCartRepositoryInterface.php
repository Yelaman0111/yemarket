<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\GetShoppingCartRequest;

interface ShoppingCartRepositoryInterface
{
    public function getShoppingCart(GetShoppingCartRequest $request);
}