<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\ProductCompanyChangeApprovedstatusRequest;
use App\Http\Requests\ProductCompanyUpdateRequest;
use App\Http\Requests\ProductConnectRequest;
use App\Models\Company;
use App\Models\ProductCompany;

interface ProductCompanyRepositoryInterface
{
    public function getCompanyProducts($company_id);
    
    public function changeApproveStatus($status, ProductCompany $productCompany);
    
    public function checkExists($product_id, $company_id );

    public function storeProductCompany(ProductConnectRequest $request, $company_id);

    public function updateProductCompany(ProductCompanyUpdateRequest $request, ProductCompany $productCompany);

    public function getCompanyIdByCompanyProductId($product_company_id);

    public function getProductCompanyById($product_company_id);
}