<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;

interface CategoryRepositoryInterface
{
    public function store(CategoryRequest $request);

    public function update(CategoryRequest $request, Category $category);

    public function destroy(Category $category);

    public function getParentCategoriesWithChildWithProductCount();

    public function getParentCategoriesWithChildCategories();

    public function getCategoriesHasProducts();

    public function getParentCategories();
}