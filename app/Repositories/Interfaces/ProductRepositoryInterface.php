<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\ProductChangeApproveStatusRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\SearchProductOfCompanyRequest;
use App\Http\Requests\ShopChangeBlockStatusRequest;
use App\Models\Product;
use App\Models\Shop;

interface ProductRepositoryInterface
{
    public function changeApproveStatus(ProductChangeApproveStatusRequest $request, Product $product);

    public function getProductsWithCategoriesWithCompanyProduct();

    public function storeProduct(ProductStoreRequest $request);

    public function searchProductOfCompany($search_text, $company_id);

    public function searchProductForConnect($search_text);

    public function getApprovedProducts();

    public function getCompanyProducts($company_id);

    public function getProductsByCategory($category_id);

    public function getProductById($product_id);

    public function searchProductsPublic($search_text);
    
}