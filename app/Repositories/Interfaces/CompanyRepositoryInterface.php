<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CompanyBlockRequest;
use App\Models\Category;
use App\Models\Company;

interface CompanyRepositoryInterface
{
    public function getCompaniesWithProductsCountAndOrdersCount();

    public function changeBlockStatus(CompanyBlockRequest $request, Company $company);
}