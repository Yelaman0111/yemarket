<?php

namespace App\Repositories;

use App\Http\Requests\ShopChangeBlockStatusRequest;
use App\Models\Shop;
use App\Repositories\Interfaces\ShopRepositoryInterface;

class ShopRepository implements ShopRepositoryInterface
{

    public function changeBlockStatus(ShopChangeBlockStatusRequest $request, Shop $shop)
    {
        $shop->blocked = $request->block_status;
        $shop->save();

        return $shop;
    }

    public function getAllShopsWithOrdersCount()
    {
        return Shop::withCount('orders')->get();

    }
}
