<?php

namespace App\Repositories;

use App\Http\Requests\GetShoppingCartRequest;
use App\Models\Company;
use App\Repositories\Interfaces\ShoppingCartRepositoryInterface;


class ShoppingCartRepository implements ShoppingCartRepositoryInterface
{

    public function getShoppingCart(GetShoppingCartRequest $request)
    {
        return Company::whereHas('productsCompany', function ($query) use ($request) {
            $query->whereIn('id', explode(',', $request->products_id));
        })->with(['productsCompany' => function ($query) use ($request) {
            $query->whereIn('id', explode(',', $request->products_id));
            $query->with('product');
        }])
            ->get();
    }
}
