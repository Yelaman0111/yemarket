<?php

namespace App\Repositories;

use App\Http\Requests\ProductChangeApproveStatusRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\SearchProductOfCompanyRequest;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function changeApproveStatus(ProductChangeApproveStatusRequest $request, Product $product)
    {
        $product->approved = $request->approved_status;
        $product->save();

        return  $product;
    }
    public function getProductsWithCategoriesWithCompanyProduct()
    {
        return Product::with('category')
            ->with(['companiesProduct' => function ($query) {
                $query->where('approved', 1);
                $query->orderBy('price', 'asc');
                $query->with('company');
            }])->paginate();
    }

    public function storeProduct(ProductStoreRequest $request)
    {
        $product = new Product();
        $imageName = 'product.jpg';

        if ($image = $request->file('image')) {
            $imageName = time() . "." . $image->extension();
            $image->move(public_path('uploads/products'), $imageName);
        }

        $product->category_id       =  $request->category_id;
        $product->name              =  $request->name;
        $product->description       =  $request->description;
        $product->image             =  $imageName;
        $product->save();

        return $product;
    }

    public function searchProductOfCompany($search_text, $company_id)
    {
        return Product::where('name', 'like', '%' . $search_text . '%')
            ->whereHas('companiesProduct', function ($query) use ($company_id) {
                $query->where('company_id',  $company_id);
            })
            ->with(['companiesProduct' => function ($query) use ($company_id) {
                $query->where('company_id', $company_id);
            }])
            ->paginate();
    }

    public function searchProductForConnect($search_text)
    {
        return Product::where('name', 'like', '%' . $search_text . '%')
            ->where('approved', '1')
            ->paginate();
    }

    public function getApprovedProducts()
    {
        return Product::where('approved',  '1')->paginate();
    }

    public function getCompanyProducts($company_id)
    {
        return Product::whereHas('companiesProduct', function ($query) use ($company_id) {
            $query->where('company_id',  $company_id);
        })
            ->with(['companiesProduct' => function ($query) use ($company_id) {
                $query->where('company_id', $company_id);
                $query->with('company');
            }])
            ->paginate();
    }

    public function getCompanyProductsPublic($company_id)
    {
        return Product::where('approved', '1')
            ->whereHas('companiesProduct', function ($query) use ($company_id) {
                $query->where('company_id',  $company_id);
            })
            ->with(['companiesProduct' => function ($query) use ($company_id) {
                $query->where('company_id', $company_id);
                $query->with('company');
            }])
            ->paginate();
    }

    public function getProductsByCategory($category_id)
    {
        return Product::where('category_id', $category_id)->where('approved', '1')
            ->whereHas('companiesProduct', function ($query) {
                $query->where('approved', '1');
            })
            ->with(['companiesProduct' => function ($query) {
                $query->orderBy('price', 'asc');
                $query->where('approved', '1');
                $query->with(['company']);
            }])->paginate(10);
    }

    public function getProductById($product_id)
    {
        return Product::where('approved', '1')
            ->where('id', $product_id)
            ->whereHas('companiesProduct', function ($query) {
                $query->where('approved', '1');
            })
            ->with(['companiesProduct.company', 'companiesProduct' => function ($query) {
                $query->orderBy('price', 'asc');
                $query->where('approved', '1');
            }])
            ->with('category')
            ->get();
    }

    public function searchProductsPublic($search_text)
    {
        return Product::where('name', 'like', '%' . $search_text . '%')
            ->where('approved', '1')
            ->whereHas('companiesProduct', function ($query) {
                $query->where('approved', '1');
            })
            ->with(['companiesProduct' => function ($query) {
                $query->orderBy('price', 'asc');
                $query->where('approved', '1');
                $query->with(['company']);
            }])
            ->paginate();
    }
}
