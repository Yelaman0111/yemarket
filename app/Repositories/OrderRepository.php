<?php

namespace App\Repositories;

use App\Contracts\OrderContract;
use App\Models\Company;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Shop;
use PhpOffice\PhpWord\PhpWord;
use App\Repositories\Interfaces\OrderRepositoryInterface;

class OrderRepository implements OrderRepositoryInterface
{

    public function getOrdersByCompany(Company $company)
    {
        return Order::where('company_id', $company->id)
            ->with('shop')
            ->with(['orderProducts' => function ($query) {
                $query->with(['productCompany' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->orderBy('created_at', 'desc')
            ->paginate();
    }

    public function getOrdersByShop(Shop $shop)
    {
        return Order::where('shop_id', $shop->id)
            ->with('company')
            ->with(['orderProducts' => function ($query) {
                $query->with(['productCompany' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->orderBy('created_at', 'desc')
            ->paginate();
    }
    public function accept(Order $order)
    {
        $order->status = OrderContract::STATUS_ACCEPTED;
        $order->save();
        return $order;
    }

    public function checkOrderOwner($company_id, $order_id)
    {
        return Order::where('id', $order_id)->where('company_id', $company_id)->exists();
    }

    public function getOrderByIdAndCompanyId($company_id, $order_id)
    {
        return  Order::where('id', $order_id)
            ->where('company_id', $company_id)
            ->with('company', 'shop')
            ->with(['orderProducts' => function ($query) {
                $query->with(['productCompany' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->first();
    }

    // public function download(Order $order)
    // {
    //     $company_id = auth()->guard('company-api')->user()->id;

    //     $companyOrder = $this->getOrderByIdAndCompanyId($company_id, $order->id);
 
    //     $phpWord = new PhpWord();
    //     $section = $phpWord->addSection();
    //     $boldFontStyle = array('bold' => true);

    //     $title = "Расходная накладная";
    //     $section->addText($title, $boldFontStyle, array('align' => 'center'));
    //     $section->addTextBreak();

    //     $section->addText('Поставщик:', null, array('align' => 'left'));
    //     $section->addText(htmlspecialchars($companyOrder->company->name), $boldFontStyle, array('align' => 'left'));
    //     $section->addText(htmlspecialchars($companyOrder->company->phone), null, array('align' => 'left'));

    //     $section->addTextBreak();
    //     $section->addTextBreak();

    //     $section_style = $section->getStyle();
    //     $position =
    //         $section_style->getPageSizeW()
    //         - $section_style->getMarginRight()
    //         - $section_style->getMarginLeft();

    //     $phpWord->addParagraphStyle("leftRight", array("tabs" => array(
    //         new \PhpOffice\PhpWord\Style\Tab("right", $position)
    //     )));

    //     $section->addText("Покупатель:\t Дата доставки:" . date("Y.m.d"),  $boldFontStyle, "leftRight");
    //     $section->addText(htmlspecialchars($companyOrder->shop->name . "\t Дата заказа:" . date_format($companyOrder->created_at, "Y.m.d")), array(), "leftRight");
    //     $section->addText(htmlspecialchars($companyOrder->shop->address), null, array('align' => 'left'));
    //     $section->addText(htmlspecialchars($companyOrder->shop->phone), null, array('align' => 'left'));
    //     $section->addTextBreak();

    //     //////////////////////////////
    //     //Таблица товаров
    //     $table = array('borderColor' => 'black', 'borderSize' => 1, 'cellMargin' => 50, 'valign' => 'center');
    //     $phpWord->addTableStyle('table', $table);
    //     $table = $section->addTable('table');
    //     $table->addRow();
    //     $table->addCell(800)->addText(htmlspecialchars("№ п/п"));
    //     $table->addCell(4800)->addText(htmlspecialchars("Наименование товара"));
    //     $table->addCell(1300)->addText(htmlspecialchars("Цена (тг)"));
    //     $table->addCell(1000)->addText(htmlspecialchars("Кол-во"));
    //     $table->addCell(1300)->addText(htmlspecialchars("Сумма (тг)"));

    //     $total_price = 0;
    //     foreach ($companyOrder->orderProducts as $key => $order) {
    //         $table->addRow();
    //         $table->addCell(800)->addText(htmlspecialchars($key + 1));
    //         $table->addCell(4800)->addText($order->productCompany->product->name);
    //         $table->addCell(1300)->addText(htmlspecialchars($order->price));
    //         $table->addCell(1000)->addText(htmlspecialchars($order->count));
    //         $table->addCell(1300)->addText(htmlspecialchars($order->price * $order->count));

    //         $total_price += $order->price * $order->count;
    //     }
    //     $table->addRow();
    //     $table->addCell(6900)->addText(htmlspecialchars(""));
    //     $table->addCell(1000)->addText(htmlspecialchars("Итого"));
    //     $table->addCell(1300)->addText(htmlspecialchars($total_price));
    //     //Таблица товаров конец

    //     $section->addTextBreak();
    //     $section->addTextBreak();
    //     $section->addTextBreak();
    //     $section->addText(htmlspecialchars("Принял, претензий к внешнему виду товара"), null, array('align' => 'right'));
    //     $section->addText(htmlspecialchars("и комплектности не имею."), null, array('align' => 'right'));
    //     $section->addText("Сдал___________________________\t ______________________________________",  array(), "leftRight");

    //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    //     try {
    //         $objWriter->save(storage_path('orders/Заказ №' . $order->id . '.docx'));
    //     } catch (Exception $e) {
    //         return $e;
    //     }
    //     return response()->download(storage_path('orders/Заказ №' . $order->id . '.docx'));
    // }

    public function storeOrder($company_id, $user_id)
    {
        $order                = new Order();
        $order->shop_id       = $user_id;
        $order->company_id    = $company_id;
        $order->status        = OrderContract::STATUS_CREATED;
        $order->save();

        return $order;
    }

    public function storeOrderProduct($product_id, $product_count, $product_price, $order_id)
    {
        $order_product              = new OrderProduct();
        $order_product->order_id    = $order_id;
        $order_product->product_id  = $product_id;
        $order_product->count       = $product_count;
        $order_product->price       = $product_price;
        $order_product->save();

        return $order_product;
    }

    public function updateStatus(Order $order, $status)
    {
        $order->status = $status;
        $order->save();
        
        return $order;
    }

}