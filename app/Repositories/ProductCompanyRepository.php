<?php

namespace App\Repositories;

use App\Contracts\ProductCompanyContract;
use App\Http\Requests\ProductCompanyChangeApprovedstatusRequest;
use App\Http\Requests\ProductCompanyUpdateRequest;
use App\Http\Requests\ProductConnectRequest;
use App\Models\Company;
use App\Models\ProductCompany;
use App\Repositories\Interfaces\ProductCompanyRepositoryInterface;

class ProductCompanyRepository implements ProductCompanyRepositoryInterface
{

    public function getCompanyProducts($company_id)
    {
        return ProductCompany::where('company_id', $company_id)->with('product')->paginate();
    }

    public function changeApproveStatus($status, ProductCompany $productCompany)
    {
        $productCompany->{ProductCompanyContract::FIELD_APPROVED} = $status;
        $productCompany->save();
        return $productCompany;
    }

    public function checkExists($product_id, $company_id )
    {
        return ProductCompany::where('company_id', $company_id)->where('product_id', $product_id)->first();
    }

    public function storeProductCompany(ProductConnectRequest $request, $company_id)
    {
        $product_company              = new ProductCompany();
        $product_company->company_id    = $company_id;
        $product_company->product_id  = $request->product_id;
        $product_company->price       = $request->price;
        $product_company->sku       = $request->sku;
        $product_company->save();

        return $product_company;
       
    }

    public function updateProductCompany(ProductCompanyUpdateRequest $request, ProductCompany $productCompany)
    {
        $productCompany->price  = $request->price;
        $productCompany->sku    = $request->sku;
        $productCompany->save();

        return $productCompany;
    }

    public function getCompanyIdByCompanyProductId($product_company_id)
    {
        return ProductCompany::where('id', $product_company_id)->pluck('company_id')->first();
    }

    public function getProductCompanyById($product_company_id)
    {
        return ProductCompany::where('id', $product_company_id)->first();
    }

}
