<?php

namespace App\Repositories;

use App\Http\Requests\CompanyBlockRequest;
use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{

    public function getCompaniesWithProductsCountAndOrdersCount()
    {
        return Company::withCount('productsCompany')->withCount('orders')->paginate();
    }

    public function changeBlockStatus(CompanyBlockRequest $request, Company $company)
    {
        $company->blocked = $request->block_status;
        $company->save();
        return  $company;
    }

}
