<?php

namespace App\Policies;

use App\Models\Company;
use App\Models\Order;
use App\Models\Shop;

class OrderPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function companyAccess(Company $company, Order $order): bool
    {
        return $company->id === $order->company_id;
    }

    public function shopAccess(Shop $shop, Order $order): bool
    {
        return $shop->id === $order->shop_id;
    }

}
