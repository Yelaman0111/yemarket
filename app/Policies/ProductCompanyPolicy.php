<?php

namespace App\Policies;

use App\Models\Company;
use App\Models\ProductCompany;

class ProductCompanyPolicy
{
    /**
     * Create a new policy instance.
     */
    public function companyAccess(Company $company, ProductCompany $productCompany): bool
    {
        return $company->id === $productCompany->company_id;
    }
}
