<?php

namespace App\Contracts;

interface OrderContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_COMPANY_ID   = 'company_id';
    public const FIELD_SHOP_ID      = 'shop_id';
    public const FIELD_STATUS       = 'status';

    public const FILLABLE = [
        self::FIELD_COMPANY_ID,
        self::FIELD_SHOP_ID,
        self::FIELD_STATUS,
    ];

    public const STATUS_CANCELED    = 0;
    public const STATUS_CREATED     = 1;
    public const STATUS_ACCEPTED    = 2;
    public const STATUS_FINISHED    = 3;
}
