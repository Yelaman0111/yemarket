<?php

namespace App\Contracts;

interface OrderProductContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_ORDER_ID     = 'order_id';
    public const FIELD_PRODUCT_ID   = 'product_id';
    public const FIELD_COUNT        = 'count';
    public const FIELD_PRICE        = 'price';

    public const FILLABLE = [
        self::FIELD_ORDER_ID,
        self::FIELD_PRODUCT_ID,
        self::FIELD_COUNT,
        self::FIELD_PRICE,
    ];
}
