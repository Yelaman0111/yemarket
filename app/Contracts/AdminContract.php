<?php

namespace App\Contracts;

interface AdminContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_NAME         = 'name';
    public const FIELD_EMAIL        = 'email';
    public const FIELD_PASSWORD     = 'password';

    public const FILLABLE = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
    ];

    public const HIDDEN = [
        self::FIELD_PASSWORD
    ];
}
