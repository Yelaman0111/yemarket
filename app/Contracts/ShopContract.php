<?php

namespace App\Contracts;

interface ShopContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_NAME         = 'name';
    public const FIELD_PHONE        = 'phone';
    public const FIELD_ADDRESS      = 'address';
    public const FIELD_BLOCKED      = 'blocked';
    public const FIELD_PASSWORD     = 'password';

    public const FILLABLE = [
        self::FIELD_NAME,
        self::FIELD_PHONE,
        self::FIELD_ADDRESS,
        self::FIELD_BLOCKED,
        self::FIELD_PASSWORD,
    ];

    public const HIDDEN = [
        self::FIELD_PASSWORD
    ];

    public const BLOCKED_TRUE   = 1;
    public const BLOCKED_FALSE  = 0;
}
