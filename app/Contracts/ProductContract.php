<?php

namespace App\Contracts;

interface ProductContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_CATEGORY_ID  = 'category_id';
    public const FIELD_NAME         = 'name';
    public const FIELD_DESCRIPTIONS = 'description';
    public const FIELD_IMAGE        = 'image';
    public const FIELD_APPROVED     = 'approved';

    public const FILLABLE = [
        self::FIELD_CATEGORY_ID,
        self::FIELD_NAME,
        self::FIELD_DESCRIPTIONS,
        self::FIELD_IMAGE,
        self::FIELD_APPROVED,
    ];

    public const APPROVED_TRUE   = 1;
    public const APPROVED_FALSE  = 0;
}