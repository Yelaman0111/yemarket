<?php

namespace App\Contracts;

interface CategoryContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_PARENT_ID    = 'parent_id';
    public const FIELD_TITLE        = 'title';
    public const FIELD_IMAGE        = 'image';

    public const FILLABLE = [
        self::FIELD_PARENT_ID,
        self::FIELD_TITLE,
        self::FIELD_IMAGE,
    ];
}
