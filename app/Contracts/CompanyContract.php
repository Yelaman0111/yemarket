<?php

namespace App\Contracts;

interface CompanyContract
{
    public const FIELD_ID               = 'id';
    public const FIELD_NAME             = 'name';
    public const FIELD_EMAIL            = 'email';
    public const FIELD_PASSWORD         = 'password';
    public const FIELD_PHONE            = 'phone';
    public const FIELD_BLOCKED          = 'blocked';
    public const FIELD_MIN_ORDER_SUM    = 'min_order_sum';

    public const FILLABLE = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_PHONE,
        self::FIELD_BLOCKED,
        self::FIELD_MIN_ORDER_SUM,
    ];
    
    public const HIDDEN = [
        self::FIELD_PASSWORD
    ];

    public const BLOCKED_TRUE   = 0;
    public const BLOCKED_FALSE  = 1;

}
