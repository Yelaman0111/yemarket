<?php

namespace App\Contracts;

interface ProductCompanyContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_COMPANY_ID   = 'company_id';
    public const FIELD_PRODUCT_ID   = 'product_id';
    public const FIELD_PRICE        = 'price';
    public const FIELD_SKU          = 'sku';
    public const FIELD_APPROVED     = 'approved';

    public const FILLABLE = [
        self::FIELD_COMPANY_ID,
        self::FIELD_PRODUCT_ID,
        self::FIELD_PRICE,
        self::FIELD_SKU,
        self::FIELD_APPROVED,
    ];

    public const APPROVED_TRUE   = 1;
    public const APPROVED_FALSE  = 0;
}
