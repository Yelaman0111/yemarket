<?php

namespace App\Services;

use App\Http\Requests\ShopChangeBlockStatusRequest;
use App\Models\Shop;
use App\Repositories\Interfaces\ShopRepositoryInterface;

class ShopService
{

    private $shopRepository;

    public function __construct(ShopRepositoryInterface $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    public function changeBlockStatus(ShopChangeBlockStatusRequest $request, Shop $shop)
    {
        return $this->shopRepository->changeBlockStatus($request, $shop);
    }

    public function getAllShopsWithOrdersCount()
    {
        return $this->shopRepository->getAllShopsWithOrdersCount();
    }

}