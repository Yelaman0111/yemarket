<?php

namespace App\Services;

use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Http\Requests\CompanyBlockRequest;
use App\Http\Requests\ProductCompanyChangeApprovedstatusRequest;
use App\Http\Requests\ProductCompanyUpdateRequest;
use App\Http\Requests\ProductConnectRequest;
use App\Models\ProductCompany;
use App\Repositories\Interfaces\ProductCompanyRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class ProductCompanyService
{

    private $productCompanyRepository;

    public function __construct(ProductCompanyRepositoryInterface $productCompanyRepository)
    {
        $this->productCompanyRepository = $productCompanyRepository;
    }

    public function getCompanyProducts(Company $company)
    {
        return $this->productCompanyRepository->getCompanyProducts($company->id);
    }

    public function changeApproveStatus(ProductCompanyChangeApprovedstatusRequest $request, ProductCompany $productCompany)
    {
        return $this->productCompanyRepository->changeApproveStatus($request->approved_status, $productCompany);
    }

    public function storeProductCompany(ProductConnectRequest $request)
    {
        $company_id = auth()->guard('company-api')->user()->id;

        $checkExists = $this->productCompanyRepository->checkExists($request->product_id, $company_id);

        if (!$checkExists) {
            return $this->productCompanyRepository->storeProductCompany($request, $company_id);
        }

        return response()->json(['error' => 'already connected'], Response::HTTP_BAD_REQUEST);
    }

    public function updateProductCompany(ProductCompanyUpdateRequest $request, ProductCompany $productCompany)
    {
        return $this->productCompanyRepository->updateProductCompany($request, $productCompany);
    }

}