<?php

namespace App\Services;

use App\Exports\PriceListExport;
use App\Imports\PriceListImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class PriceListService
{
    public function exportPriceList()
    {
        $points = new PriceListExport();
        return Excel::download($points, 'pricelist.xlsx');
    }

    public function importPriceList(Request $request)
    {
       Excel::import(new PriceListImport, $request->file('file'));
    }
}