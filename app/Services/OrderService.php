<?php

namespace App\Services;

use App\Contracts\OrderContract;
use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Http\Requests\CompanyBlockRequest;
use App\Http\Requests\OrderAcceptRequest;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;
use App\Models\Shop;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Repositories\Interfaces\ProductCompanyRepositoryInterface;
use App\Repositories\ProductCompanyRepository;
use Symfony\Component\HttpFoundation\Response;
use PhpOffice\PhpWord\PhpWord;
class OrderService
{

    private $orderRepository;
    private $productCompanyRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, ProductCompanyRepositoryInterface $productCompanyRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->productCompanyRepository = $productCompanyRepository;
    }

    public function getOrdersByCompany(Company $company)
    {
        return $this->orderRepository->getOrdersByCompany($company);
    }

    public function getOrdersByShop(Shop $shop)
    {
        return $this->orderRepository->getOrdersByShop($shop);
    }

    public function accept(Order $order)
    {
        return $this->orderRepository->accept($order);
    }

    public function getOrdersOfCompany()
    {
        $company = Company::where('id', auth()->guard('company-api')->user()->id)->first();

        return $this->orderRepository->getOrdersByCompany($company);
    }

    public function downloadOrder(Order $order)
    {
        $company_id = auth()->guard('company-api')->user()->id;

        $companyOrder = $this->orderRepository->getOrderByIdAndCompanyId($company_id, $order->id);

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        $boldFontStyle = array('bold' => true);

        $title = "Расходная накладная";
        $section->addText($title, $boldFontStyle, array('align' => 'center'));
        $section->addTextBreak();

        $section->addText('Поставщик:', null, array('align' => 'left'));
        $section->addText(htmlspecialchars($companyOrder->company->name), $boldFontStyle, array('align' => 'left'));
        $section->addText(htmlspecialchars($companyOrder->company->phone), null, array('align' => 'left'));

        $section->addTextBreak();
        $section->addTextBreak();

        $section_style = $section->getStyle();
        $position =
            $section_style->getPageSizeW()
            - $section_style->getMarginRight()
            - $section_style->getMarginLeft();

        $phpWord->addParagraphStyle("leftRight", array("tabs" => array(
            new \PhpOffice\PhpWord\Style\Tab("right", $position)
        )));

        $section->addText("Покупатель:\t Дата доставки:" . date("Y.m.d"),  $boldFontStyle, "leftRight");
        $section->addText(htmlspecialchars($companyOrder->shop->name . "\t Дата заказа:" . date_format($companyOrder->created_at, "Y.m.d")), array(), "leftRight");
        $section->addText(htmlspecialchars($companyOrder->shop->address), null, array('align' => 'left'));
        $section->addText(htmlspecialchars($companyOrder->shop->phone), null, array('align' => 'left'));
        $section->addTextBreak();

        //////////////////////////////
        //Таблица товаров
        $table = array('borderColor' => 'black', 'borderSize' => 1, 'cellMargin' => 50, 'valign' => 'center');
        $phpWord->addTableStyle('table', $table);
        $table = $section->addTable('table');
        $table->addRow();
        $table->addCell(800)->addText(htmlspecialchars("№ п/п"));
        $table->addCell(4800)->addText(htmlspecialchars("Наименование товара"));
        $table->addCell(1300)->addText(htmlspecialchars("Цена (тг)"));
        $table->addCell(1000)->addText(htmlspecialchars("Кол-во"));
        $table->addCell(1300)->addText(htmlspecialchars("Сумма (тг)"));

        $total_price = 0;
        foreach ($companyOrder->orderProducts as $key => $order) {
            $table->addRow();
            $table->addCell(800)->addText(htmlspecialchars($key + 1));
            $table->addCell(4800)->addText($order->productCompany->product->name);
            $table->addCell(1300)->addText(htmlspecialchars($order->price));
            $table->addCell(1000)->addText(htmlspecialchars($order->count));
            $table->addCell(1300)->addText(htmlspecialchars($order->price * $order->count));

            $total_price += $order->price * $order->count;
        }
        $table->addRow();
        $table->addCell(6900)->addText(htmlspecialchars(""));
        $table->addCell(1000)->addText(htmlspecialchars("Итого"));
        $table->addCell(1300)->addText(htmlspecialchars($total_price));
        //Таблица товаров конец

        $section->addTextBreak();
        $section->addTextBreak();
        $section->addTextBreak();
        $section->addText(htmlspecialchars("Принял, претензий к внешнему виду товара"), null, array('align' => 'right'));
        $section->addText(htmlspecialchars("и комплектности не имею."), null, array('align' => 'right'));
        $section->addText("Сдал___________________________\t ______________________________________",  array(), "leftRight");

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path('orders/Заказ №' . $companyOrder->id . '.docx'));
        } catch (Exception $e) {
            return $e;
        }
        return response()->download(storage_path('orders/Заказ №' . $companyOrder->id . '.docx'));


        // return $this->orderRepository->download($order);
    }

    public function getOrdersOfShop()
    {
        $shop = Shop::where('id', auth()->guard('shop-api')->user()->id)->first();

        return $this->orderRepository->getOrdersByShop($shop);
    }

    public function storeOrder(OrderStoreRequest $request)
    {
        $products_id = explode(',', $request->products_id);
        $products_count = explode(',', $request->products_count);
        $shop_id = auth()->guard('shop-api')->user()->id;

        $company_id = $this->productCompanyRepository->getCompanyIdByCompanyProductId($products_id[0]);
        $order = $this->orderRepository->storeOrder($company_id, $shop_id);

        foreach ($products_id as $key => $product_id) {
            $product_company = $this->productCompanyRepository->getProductCompanyById($product_id);
            $this->orderRepository->storeOrderProduct($product_id, $products_count[$key], $product_company->price, $order['id']);
        }
    }

    public function cancelOrder(Order $order)
    {
        return $this->orderRepository->updateStatus($order, OrderContract::STATUS_CANCELED);
    }

    public function confirmOrder(Order $order)
    {
        return $this->orderRepository->updateStatus($order, OrderContract::STATUS_FINISHED);
    }
}
