<?php

namespace App\Services;

use App\Http\Requests\GetShoppingCartRequest;
use App\Repositories\Interfaces\ShoppingCartRepositoryInterface;

class ShoppingCartService
{

    private $shoppingCartRepository;

    public function __construct(ShoppingCartRepositoryInterface $shoppingCartRepository)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    public function getShoppingCart(GetShoppingCartRequest $request)
    {
        return $this->shoppingCartRepository->getShoppingCart($request);
    }

}