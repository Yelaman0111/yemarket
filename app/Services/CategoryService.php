<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryService
{

    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getParentCategoriesWithChildWithProductCount()
    {
        return $this->categoryRepository->getParentCategoriesWithChildWithProductCount();
    }

    public function store(CategoryRequest $request)
    {
        return $this->categoryRepository->store($request);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        return $this->categoryRepository->update($request, $category);
    }

    public function destroy(Category $category)
    {
        return $this->categoryRepository->destroy($category);
    }

    public function getParentCategoriesWithChildCategories()
    {
        return $this->categoryRepository->getParentCategoriesWithChildCategories();
    }

    public function getCategoriesHasProducts()
    {
        return $this->categoryRepository->getCategoriesHasProducts();

    }

    public function getParentCategories()
    {
        return $this->categoryRepository->getParentCategories();

    }
}