<?php

namespace App\Services;

use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Http\Requests\CompanyBlockRequest;
use App\Http\Requests\ProductChangeApproveStatusRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductService
{

    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function changeApproveStatus(ProductChangeApproveStatusRequest $request, Product $product)
    {
        return $this->productRepository->changeApproveStatus($request, $product);
    }

    public function getProductsWithCategoriesWithCompanyProduct()
    {
        return $this->productRepository->getProductsWithCategoriesWithCompanyProduct();
    }

    public function storeProduct(ProductStoreRequest $request)
    {
        return $this->productRepository->storeProduct($request);
    }

    public function searchProductOfCompany($search_text)
    {
        $company_id = auth()->guard('company-api')->user()->id;

        return $this->productRepository->searchProductOfCompany($search_text, $company_id);
    }

    public function searchProductForConnect($search_text)
    {
        return $this->productRepository->searchProductForConnect($search_text);
    }

    public function getApprovedProducts()
    {
        return $this->productRepository->getApprovedProducts();
    }

    public function getCompanyProducts()
    {
        $company_id = auth()->guard('company-api')->user()->id;

        return $this->productRepository->getCompanyProducts($company_id);
    }

    public function getProductsByCategory($category_id)
    {
        return $this->productRepository->getProductsByCategory($category_id);
    }

    public function getProductsByCompany($company_id)
    {
        return $this->productRepository->getCompanyProducts($company_id);
    }

    public function getProductById($product_id)
    {
        return $this->productRepository->getProductById($product_id);
    }

    public function searchProductsPublic($search_text)
    {
        return $this->productRepository->searchProductsPublic($search_text);

    }
}
