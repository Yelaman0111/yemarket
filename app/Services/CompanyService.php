<?php

namespace App\Services;

use App\Contracts\ProductCompanyContract;
use App\Models\Company;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Http\Requests\CompanyBlockRequest;
use App\Repositories\Interfaces\ProductCompanyRepositoryInterface;

class CompanyService
{

    private $companyRepository;
    private $productCompanyRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository, ProductCompanyRepositoryInterface $productCompanyRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->productCompanyRepository = $productCompanyRepository;
    }

    public function getCompaniesWithProductsCountAndOrdersCount()
    {
        return $this->companyRepository->getCompaniesWithProductsCountAndOrdersCount();
    }

    public function changeBlockStatus(CompanyBlockRequest $request, Company $company)
    {

        $company_products = $this->productCompanyRepository->getCompanyProducts($company);

        foreach ($company_products->productsCompany as $productsCompany) {
            $this->productCompanyRepository->changeApproveStatus(ProductCompanyContract::APPROVED_FALSE, $productsCompany);
        }

        return $this->companyRepository->changeBlockStatus($request, $company);
    }
}
