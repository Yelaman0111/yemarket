<?php

namespace App\Imports;

use App\Models\ProductCompany;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithUpserts;

class PriceListImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            ProductCompany::where('company_id', auth()->guard('company-api')->user()->id)
                ->where('sku', $row[1])
                ->update([
                    'price'     => $row[2]
                ]);
        }
    }
}
