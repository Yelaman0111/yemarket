<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\OrderResource;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;
use Symfony\Component\HttpFoundation\Response;

class ShopController extends Controller
{
    //Order
    public function getShopOrders(OrderService $service)
    {
        return new OrderResource($service->getOrdersOfShop());
    }

    public function storeShopOrder(OrderStoreRequest $request, OrderService $service)
    {
        $service->storeOrder($request);
        return response()->json(['message' => 'success'], Response::HTTP_CREATED);
    }

    public function cancelShopOrder(Order $order, OrderService $service)
    {
        return new OrderResource($service->cancelOrder($order));
    }

    public function confirmShopOrder(Order $order, OrderService $service)
    {
        return new OrderResource($service->confirmOrder($order));
    }
    //Order

}
