<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\CompanyLoginRequest;
use App\Http\Requests\Auth\CompanyRegisterRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthCompanyController extends Controller
{

    public function register(CompanyRegisterRequest $request)
    {
        $company = new Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->password = Hash::make($request->password);
        $company->phone = $request->phone;
        $company->min_order_sum = $request->min_order_sum;
        $company->save();

        return new CompanyResource($company);
    }

    public function login(CompanyLoginRequest $request)
    {
        $credentials = $request->all();
        $myTTL = 60 * 24 * 30; //minutes

        JWTAuth::factory()->setTTL($myTTL);
        if (!$token = auth()->guard('company-api')->attempt($credentials, ['exp' => Carbon::now()->addDays(30)->timestamp])) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_UNAUTHORIZED);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return new CompanyResource(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 60 * 24 * 30
        ]);
    }

    public function update(CompanyUpdateRequest $request)
    {
        $company = Company::find(auth()->guard('company-api')->user()->id);
        $company->name = $request->name;
        $company->password = Hash::make($request->password);
        $company->phone = $request->phone;
        $company->min_order_sum = $request->min_order_sum;
        $company->save();
        return new CompanyResource($company);
    }

}
