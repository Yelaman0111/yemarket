<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ShopLoginRequest;
use App\Http\Requests\Auth\ShopRegisterRequest;
use App\Http\Resources\ShopResource;
use App\Models\Shop;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthShopController extends Controller
{
    public function register(ShopRegisterRequest $request)
    {
        $shop = new Shop();
        $shop->name = $request->name;
        $shop->password = Hash::make($request->password);
        $shop->phone = $request->phone;
        $shop->address = $request->address;
        $shop->save();

        return new ShopResource($shop);
    }

    public function login(ShopLoginRequest $request)
    {
        $credentials = $request->all();
        $myTTL = 60 * 24 * 30; //minutes

        JWTAuth::factory()->setTTL($myTTL);

        $shop = Shop::where('phone', $request->phone)->first();

        if($shop->blocked){
            return response()->json(['error' => 'Blocked'], Response::HTTP_UNAUTHORIZED);
        }

        if (!$token = auth()->guard('shop-api')->attempt($credentials, ['exp' => Carbon::now()->addDays(30)->timestamp])) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_UNAUTHORIZED);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function destroy()
    {
        $shop = Shop::find(auth()->user()->id);
        $shop->delete();

        return response()->json(['message' => 'Successfully deleted']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => 60 * 24 * 30
        ]);
    }
}
