<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CompanyBlockRequest;
use App\Http\Requests\ProductChangeApproveStatusRequest;
use App\Http\Requests\ProductCompanyChangeApprovedstatusRequest;
use App\Http\Requests\ShopChangeBlockStatusRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductCompanyResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ShopResource;
use App\Models\Category;
use App\Models\Company;
use App\Models\Product;
use App\Models\ProductCompany;
use App\Models\Shop;
use App\Services\CategoryService;
use App\Services\CompanyService;
use App\Services\OrderService;
use App\Services\ProductCompanyService;
use App\Services\ProductService;
use App\Services\ShopService;

class AdminController extends Controller
{
    //CATEGORIES
    public function getParentCategoriesWithChildWithProductCount(CategoryService $service)
    {
        return new CategoryResource($service->getParentCategoriesWithChildWithProductCount());
    }

    public function getParentCategories(CategoryService $service)
    {
        return new CategoryResource($service->getParentCategories());
    }

    public function storeCategory(CategoryRequest $request, CategoryService $service)
    {
        return new CategoryResource($service->store($request));
    }

    public function updateCategory(CategoryRequest $request, Category $category, CategoryService $service)
    {
        return new CategoryResource($service->update($request, $category));
    }

    public function destroyCategory(Category $category, CategoryService $service)
    {
        $service->destroy($category);
        return response()->noContent();
    }
    //CATEGORIES END

    //COMPANIES

    public function getCompaniesWithProductsCountAndOrdersCount(CompanyService $service)
    {
        return new CompanyResource($service->getCompaniesWithProductsCountAndOrdersCount());
    }

    public function blockCompany(CompanyBlockRequest $request, Company $company, CompanyService $service)
    {
        return new CompanyResource($service->changeBlockStatus($request, $company));
    }

    public function getCompanyProducts(Company $company, ProductCompanyService $service)
    {
        return new ProductResource($service->getCompanyProducts($company));
    }

    public function getCompanyOrders(Company $company, OrderService $service)
    {
        return new OrderResource($service->getOrdersByCompany($company));
    }

    //COMPANY PRODUCTS 
    public function approveCompaniesProducts(ProductCompanyChangeApprovedstatusRequest $request, ProductCompany $productCompany, ProductCompanyService $service)
    {
        return new ProductCompanyResource($service->changeApproveStatus($request, $productCompany));
    }
    //COMPANY PRODUCTS END 

    //COMPANIES END

    //SHOPS
    public function blockShop(ShopChangeBlockStatusRequest $request, Shop $shop, ShopService $service)
    {
        return new ShopResource($service->changeBlockStatus($request, $shop));
    }

    public function getShopOrders(Shop $shop, OrderService $service)
    {
        return new OrderResource($service->getOrdersByShop($shop));
    }

    public function getAllShopsWithOrdersCount(ShopService $service)
    {
        return new ShopResource($service->getAllShopsWithOrdersCount());
    }
    //SHOPS END

    //PRODUCTS
    public function approveProduct(ProductChangeApproveStatusRequest $request, Product $product, ProductService $service)
    {
        return new ProductResource($service->changeApproveStatus($request,  $product));
    }

    public function getProductsWithCategoriesWithCompanyProduct(ProductService $service)
    {
        return new ProductResource($service->getProductsWithCategoriesWithCompanyProduct());
    }

    //PRODUCTS END

}
