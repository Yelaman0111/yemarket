<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderAcceptRequest;
use App\Http\Requests\ProductCompanyUpdateRequest;
use App\Http\Requests\ProductConnectRequest;
use App\Http\Requests\SearchProductOfCompanyRequest;
use App\Http\Requests\UpdateProductCompanyRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductCompanyResource;
use App\Models\Order;
use App\Models\ProductCompany;
use App\Services\OrderService;
use App\Services\PriceListService;
use App\Services\ProductCompanyService;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Http\FormRequest;

class CompanyController extends Controller
{
    //Category
    public function getParentCategoriesWithChildCategories(CategoryService $service)
    {
        return CategoryResource::collection($service->getParentCategoriesWithChildCategories());
    }
    //Category

    //Product
    public function storeProduct(ProductStoreRequest $request, ProductService $service)
    {
        return new ProductResource($service->storeProduct($request));
    }
    //Product

    //Product Company
    public function connectToProduct(ProductConnectRequest $request, ProductCompanyService $service)
    {
        return $service->storeProductCompany($request);

    }
  
    public function searchProductOfCompany(SearchProductOfCompanyRequest $request, ProductService $service){
        return new ProductResource($service->searchProductOfCompany($request->search_text));
    }

    public function searchProductForConnect(SearchProductOfCompanyRequest $request, ProductService $service)
    {
        return new ProductResource($service->searchProductForConnect($request->search_text));
    }

    public function getApprovedProducts(ProductService $service)
    {
        return new ProductResource($service->getApprovedProducts());
    }

    public function getCompanyProducts(ProductService $service)
    {
        return new ProductResource($service->getCompanyProducts());
    }

    public function updateProductCompany(ProductCompanyUpdateRequest $request, ProductCompany $productCompany, ProductCompanyService $service)
    {
        return new ProductCompanyResource($service->updateProductCompany($request, $productCompany));
    }

    //Product Company


    // Order
    public function orderAccept(Order $order, OrderService $service)
    {
        return $service->accept($order);
    }

    public function getCompanyOrders(OrderService $service)
    {
        return new OrderResource($service->getOrdersOfCompany());
    }

    public function downloadCompanyOrder(Order $order, OrderService $service)
    {
        return $service->downloadOrder($order);
    }

    // Order

    //Price list
    public function exportPriceList(PriceListService $service)
    {
        return $service->exportPriceList();
    }

    public function importPriceList(FormRequest $request, PriceListService $service)
    {
        // if ($image = $request->file('image')) {
        //     $imageName = time() . "." . $image->extension();
        //     $image->move(public_path('uploads/products'), $imageName);
        // }

         
        // return request()->all();
        // $validator = Validator::make(
		// 	$request->all(),
		// 	[
		// 		'file' => 'required|mimes:xls,xlsx|max:8192',
		// 	]
		// );

		// if ($validator->fails()) {
		// 	return response()->json(['error' => $validator->errors()], 401);
		// }
        // if ($image = $request->file('file')) {
        //     $imageName = time() . "." . $image->extension();
        //     $image->move(public_path('uploads/products'), $imageName);
        // }
        // return $image;
        // return request()->file('pricelist.xlsx');
        return $service->importPriceList( request());
    }
    //Price list

}
