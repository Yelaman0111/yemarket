<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetShoppingCartRequest;
use App\Http\Requests\SearchProductPublicRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Company;
use App\Models\Product;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\ShoppingCartService;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function getCategories(CategoryService $service)
    {
        return new CategoryResource($service->getCategoriesHasProducts());
    }

    public function getProductsByCategory(Category $category, ProductService $service)
    {
        return new ProductResource($service->getProductsByCategory($category->id));
    }

    public function getProductsByCompany(Company $company, ProductService $service)
    {
        return new ProductResource($service->getProductsByCompany($company->id));
    }

    public function getProduct(Product $product, ProductService $service)
    {
        return new ProductResource($service->getProductById($product->id));
    }

    public function searchProduct(SearchProductPublicRequest $request, ProductService $service)
    {
        return new ProductResource($service->searchProductsPublic($request->search_text));
    }

    public function getShoppingCart(GetShoppingCartRequest $request, ShoppingCartService $service)
    {
        return new CompanyResource($service->getShoppingCart($request));
    }
}
