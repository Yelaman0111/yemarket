<?php

namespace App\Models;

use App\Contracts\CategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model implements CategoryContract
{
    use HasFactory, SoftDeletes;

    protected $fillable = self::FILLABLE;

    public function parent()
    {
        return $this->belongsTo(Category::class, self::FIELD_PARENT_ID);
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    // recursive, loads all descendants
    public function childCategories()
    {
        return $this->children()->with('childCategories');
    }
}