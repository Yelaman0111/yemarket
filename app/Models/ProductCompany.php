<?php

namespace App\Models;

use App\Contracts\ProductCompanyContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCompany extends Model implements ProductCompanyContract
{
    use HasFactory, SoftDeletes;

    protected $fillable = self::FILLABLE;

    public function product() 
    {
       return $this->belongsTo(Product::class, self::FIELD_PRODUCT_ID, self::FIELD_ID);
    }
    
    public function company() 
    {
       return $this->belongsTo(Company::class, self::FIELD_COMPANY_ID, self::FIELD_ID);
    }
}
