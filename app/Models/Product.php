<?php

namespace App\Models;

use App\Contracts\ProductContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements ProductContract
{
    use HasFactory, SoftDeletes;

    protected $fillable = self::FILLABLE;

    public function companiesProduct()
    {
        return $this->hasMany(ProductCompany::class, 'product_id', 'id');
    }
    public function category() 
    {
       return $this->belongsTo(Category::class, self::FIELD_CATEGORY_ID, 'id');
    }
}
