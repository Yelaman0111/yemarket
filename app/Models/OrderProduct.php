<?php

namespace App\Models;

use App\Contracts\OrderProductContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model implements OrderProductContract
{
    use HasFactory, SoftDeletes;

    protected $fillable = self::FILLABLE;


    public function order() 
    {
       return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    public function productCompany() 
    {
       return $this->belongsTo(ProductCompany::class, 'product_id', 'id');
    }

}
