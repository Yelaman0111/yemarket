<?php

namespace App\Models;

use App\Contracts\ShopContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Shop extends Authenticatable implements ShopContract, JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $fillable = self::FILLABLE;
    protected $hidden = self::HIDDEN;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'shop_id');
    }
}
