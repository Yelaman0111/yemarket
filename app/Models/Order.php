<?php

namespace App\Models;

use App\Contracts\OrderContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model implements OrderContract
{
    use HasFactory, SoftDeletes;

    protected $fillable = self::FILLABLE;

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }

    public function company() 
    {
       return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function shop() 
    {
       return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }
}
