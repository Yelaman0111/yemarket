<?php

namespace App\Models;

use App\Contracts\CompanyContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Company extends Authenticatable implements CompanyContract, JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = self::FILLABLE;
    protected $hidden = self::HIDDEN;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'company_id');
    }

    public function productsCompany()
    {
        return $this->hasMany(ProductCompany::class, 'company_id');
    }

}
