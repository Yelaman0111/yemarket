<?php

namespace App\Exports;

use App\Contracts\ProductCompanyContract;
use App\Http\Resources\ProductCompanyResource;
use App\Models\ProductCompany;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PriceListExport implements FromCollection, WithHeadings, WithColumnWidths, WithMapping
{

    public function collection()
    {
        return ProductCompany::select('products.name', 'sku', 'price', 'product_companies.approved')
            ->join('products', 'products.id', '=', 'product_companies.product_id')
            ->where('company_id', auth()->guard('company-api')->user()->id)
            ->where('products.approved', 1)
            ->get();
    }

    public function headings(): array
    {
        return [
            'Название',
            'SKU',
            'Цена',
            'Статус',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 35,
            'B' => 20,
            'C' => 20,
            'D' => 20,
        ];
    }

    public function map($row): array
    {
        return [
            $row->name,
            $row->sku,
            $row->price,
            $row->approved ? 'Активен' : 'Не активен' ,
        ];
    }
}
