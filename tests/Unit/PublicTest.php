<?php

namespace Tests\Unit;

use Tests\TestCase;

class PublicTest extends TestCase
{

    public function test_categories(): void
    {
        $this->getJson(route('public.categories'))->assertOk();
    }

    public function test_category_products(): void
    {
        $category = $this->getJson(route('public.categories'))->assertOk();
        $this->getJson(route('public.categories.products', $category['data'][0]['child_categories'][0]['id']))->assertOk();
    }

    public function test_company_products(): void
    {
        $this->getJson(route('public.company.products', 1))->assertOk();
    }

    public function test_get_product(): void
    {
        $company_products = $this->getJson(route('public.company.products', 1))->assertOk();
        $this->getJson(route('public.product', $company_products['data'][0]['id']))->assertOk();
    }

    public function test_search_product(): void
    {
        $this->postJson(route('public.search'), [
            'search_text' => 'summer'
        ])->assertOk();
    }
}
