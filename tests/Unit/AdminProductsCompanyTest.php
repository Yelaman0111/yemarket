<?php

namespace Tests\Unit;
use Tests\TestCase;

class AdminProductsCompanyTest extends AdminAuthTestCase
{
   
    public function test_approve_companies_product(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.companies.with.products.orders.count'))->assertOk();
        $products = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.company.products', $response['data'][0]['id']))->assertOk();

        $product_id = $products['data'][0]['product_id'];


        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.company.product.approve', $product_id),
            ['approved_status' => 1]
        )->assertOk();

        $this->assertDatabaseHas('product_companies', [
            'id' => $product_id,
            'approved'  => 1
        ]);
    }
}
