<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Models\ProductCompany;
use Tests\Unit\CompanyAuthTestCase;

class CompanyProductTest extends CompanyAuthTestCase
{
    public function test_store_product(): void
    {
        $product = Product::factory()->make();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('company.products.store'),
            [
                'category_id' => $product->category_id,
                'name' => $product->name,
                'description' => $product->description,
            ]
        )->assertCreated();
    }

    public function test_connect_to_product()
    {
        $product_company = ProductCompany::factory()->make();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('company.products.connect'),
            [
                'product_id' => $product_company->product_id,
                'price' => $product_company->price,
                'sku' => $product_company->sku,
            ]
        )->assertCreated();
    }

    public function test_get_all_products()
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('company.products.all'))->assertOk();

    }

    public function test_get_my_products()
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('company.products.my'))->assertOk();

    }

    public function test_search_my_products()
    {
        $product_company = ProductCompany::factory()->make();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('company.products.connect'),
            [
                'product_id' => $product_company->product_id,
                'price' => $product_company->price,
                'sku' => $product_company->sku,
            ]
        )->assertCreated();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('company.products.search.my'),
            [
                'search_text' => 'Summer',
            ]
        )->assertOk();
    }

    public function test_search_all_products()
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('company.products.search.all'),
            [
                'search_text' => 'Summer',
            ]
        )->assertOk();
    }

    public function test_update_product_company()
    {
        $product_company = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('company.products.my'))->assertOk();

        $product_company_id = $product_company['data'][0]['companies_product'][0]['id'];

        $product_company_updated = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('company.products.update', $product_company_id),
            [
                'price' => '111',
                'sku' => '222',
            ]
        )->assertOk();

        $this->assertDatabaseHas('product_companies', [
            'id' => $product_company_updated['data']['id'],
            'price'  => '111',
            'sku' => '222',
        ]);
    }

}
