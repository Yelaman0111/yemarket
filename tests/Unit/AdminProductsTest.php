<?php

namespace Tests\Unit;
use Tests\TestCase;

class AdminProductsTest extends AdminAuthTestCase
{
    public function test_get_products_with_categories_with_companies(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.products'))->assertOk();
    }

    public function test_approve_products(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.products'))->assertOk();
        $product_id = $response['data'][0]['id'];


        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.products.approve',  $product_id),
            ['approved_status' => 1]
        )->assertOk();

    }
}
