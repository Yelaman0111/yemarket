<?php

namespace Tests\Unit;

use Tests\TestCase;

class CompanyAuthTestCase extends TestCase
{
   
    protected $token;

    public function setUp(): void
    {
        parent::setUp();

        $token = $this->postJson(
            route('company.login'),
            [
                'email' => 'mrodriguez@bruen.org',
                'password' => '123456',
            ]
        );

        $this->token = $token['access_token'];
    }

}
