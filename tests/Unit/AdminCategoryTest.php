<?php

namespace Tests\Unit;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Nette\Utils\ArrayList;
use PhpParser\Node\Expr\Cast\Array_;
use Tests\TestCase;

class AdminCategoryTest extends AdminAuthTestCase
{
   
    public function test_store(): void
    {
        $category = Category::factory()->make();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.store.child.category'),
            [
                'title' => $category->title,
                'parent_id' => $category->parent_id

            ]
        )->assertCreated();

        $this->assertDatabaseHas('categories', [
            'title' => $category->title,
            'parent_id'  => $category->parent_id
        ]);
    }

    public function test_update(): void
    {
        $category = Category::factory()->create();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.store.child.category', $category->id),
            [
                'title' => 'updated',
                'parent_id' => $category->parent_id
            ]
        );

        $this->assertDatabaseHas('categories', [
            'title' => 'updated',
            'parent_id'  => $category->parent_id
        ]);
    }

    public function test_destroy(): void
    {
        $category = Category::factory()->create();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->deleteJson(route('admin.destroy.category', $category->id))
        ->assertNoContent();

    }

    public function test_get_parent_categories_with_child_with_product_count():void
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.parent.category.child.product.count'))->assertOk();
    }
}
