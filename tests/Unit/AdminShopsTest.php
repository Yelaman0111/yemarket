<?php

namespace Tests\Unit;

use Tests\TestCase;

class AdminShopsTest extends AdminAuthTestCase
{
    public function test_get_shops_with_order_count(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.shops.with.order.count'))->assertOk();
    }

    public function test_block_shop(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.shops.with.order.count'))->assertOk();


        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.shops.block', $response['data'][0]['id']),
            ['block_status' => 1]
        )->assertOk();

        $this->assertDatabaseHas('shops', [
            'id' => $response['data'][0]['id'],
            'blocked'  => 1
        ]);
    }

    public function test_get_shop_orders(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.shops.with.order.count'))->assertOk();

        $orders = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.shops.orders', $response['data'][0]['id']))->assertOk();

    }
}
