<?php

namespace Tests\Unit;

use Tests\TestCase;

class ShopOrdersTest extends ShopAuthTest
{

    public function test_orders_get(): void
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('shop.orders.get'))->assertOk();
    }

    public function test_store_order(): void
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('shop.orders.store'),
            [
                'products_id' => '1',
                'products_count' => '11',
            ]
        )->assertCreated();
    }

    public function test_cancel_order()
    {
        $orders = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('shop.orders.get'))->assertOk();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('shop.orders.cancel', $orders['data'][0]['id'])
        )->assertOk();
    }

    public function test_confirm_order()
    {
        $orders = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('shop.orders.get'))->assertOk();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' .  $this->token,
        ])->postJson(
            route('shop.orders.confirm', $orders['data'][0]['id'])
        )->assertOk();
    }
}
