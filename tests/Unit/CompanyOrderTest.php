<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Unit\CompanyAuthTestCase;

class CompanyOrderTest extends CompanyAuthTestCase
{
    /**
     * A basic feature test example.
     */
    public function test_get_my_orders(): void
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('company.orders'))->assertOk();

    }

    public function test_accept_order(): void
    {
        $oreders = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('company.orders'))->assertOk();

        $order_id = $oreders['data'][0]['id'];
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(route('company.orders.accept', $order_id))->assertOk();

    }
}
