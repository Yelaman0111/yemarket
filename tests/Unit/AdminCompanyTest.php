<?php

namespace Tests\Unit;

use Tests\TestCase;

class AdminCompanyTest extends AdminAuthTestCase
{

    public function test_get_companies_with_product_and_order_count(): void
    {
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.companies.with.products.orders.count'))->assertOk();
    }

    public function test_block_company(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.companies.with.products.orders.count'))->assertOk();

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->postJson(
            route('admin.company.block', $response['data'][1]['id']),
            ['block_status' => 1]
        );

        $this->assertDatabaseHas('companies', [
            'id' => $response['data'][1]['id'],
            'blocked'  => 1
        ]);
    }

    public function test_get_company_products(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.companies.with.products.orders.count'))->assertOk();
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.company.products', $response['data'][1]['id']))->assertOk();
    }

    public function test_get_company_orders(): void
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.companies.with.products.orders.count'))->assertOk();
        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ])->getJson(route('admin.company.orders', $response['data'][1]['id']))->assertOk();
    }
}
