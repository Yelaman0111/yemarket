<?php

namespace Tests\Unit;

use Tests\TestCase;

class AdminAuthTestCase extends TestCase
{
    protected $token;

    public function setUp(): void
    {
        parent::setUp();

        $token = $this->postJson(
            route('admin.login'),
            [
                'email' => 'ebuckridge@yahoo.com',
                'password' => '0987346519',
            ]
        );

        $this->token = $token['access_token'];
    }
}
