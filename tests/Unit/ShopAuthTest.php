<?php

namespace Tests\Unit;

use Tests\TestCase;

class ShopAuthTest extends TestCase
{
    protected $token;

    public function setUp(): void
    {
        parent::setUp();

        $token = $this->postJson(
            route('shop.login'),
            [
                'phone' => '7478026928',
                'password' => '0987346519',
            ]
        );

        $this->token = $token['access_token'];
    }
}
